//Writer : Pornapon Punyapan (Na) 580611039 ISNE#3
//This version is already complete version of connect 4 game
//I have no idea how to develope more about this program , hope you enjoy the game
#include<iostream> //include libary for C++
using namespace std; //use "std::" in front all object

const int col = 9; //const size for column
const int row = 8;  //const size for row

void show_table(char [][col]); //show game table function
void player_in(char [][col],char,int []); //Player column input function
char p_turn(int); //Player turn switch function
void s_turn(char [][col]); //Switch turn function
char checK_h(char [][col],char); //Horizontally win check function
char checK_v(char [][col],char); //Vertically win check function
char checK_dup(char [][col],char); //Diagonally from up to down function
char checK_ddn(char [][col],char);//Diagonally from down to up function
int win_check(char [][col],int,char);//Win status check function
int slot_check(int); //Full slot check function

int main() //main function
{
	char table[row][col]={'-','-','-','-','-','-','-','-','-',
						'-','-','-','-','-','-','-','-','-',
						'-','-','-','-','-','-','-','-','-',
						'-','-','-','-','-','-','-','-','-',		
						'-','-','-','-','-','-','-','-','-',		//Perform '-' to all table
						'-','-','-','-','-','-','-','-','-',
						'-','-','-','-','-','-','-','-','-',
						'-','-','-','-','-','-','-','-','-'};
	
	cout<<"Welcome to Connect 4 Game !!!\n";				
	show_table(table); //Do show_table function used table[][] passed function
	s_turn(table); //Do s_turn function used table[][] passed function


	return 0; //Return 0 to main function
}

void show_table(char table[][col]) //show_table function used to displaytable
{
	int r_count=0,c_count=0,line_count=0; //r_count refer to row count c_count refer to column count and line_count usedto count to seperate line
	
	cout <<"  1   2   3   4   5   6   7 "<<endl;
	for(r_count=1;r_count<row-1; r_count++) 
	{												//table loop
		for(c_count=1;c_count<col-1; c_count++)
		{
			cout<<" ["<<table[r_count][c_count]<<"]"; //Display each column of table
			line_count = line_count+1; //total count column
			if(line_count==7) //if total column = 7
			{
				cout<<endl; //open new line
				line_count=0; //reset total column  to 0
			}
		}
	}
}

void player_in(char table[][col],char player, int slot_fill[]) //Player input function used to recieve player column select
{
	int in_col; //in_col integer refer to input column select
	
	cin >>in_col; //input column select from user
	switch(in_col) //switch the input column
	{
		case 1 : //if = 1
			if(slot_check(slot_fill[0])==1)  //check slot status if full recievenew slot select
			{								
				for(int row=6;row>=1;row--)
				{								//loop to check each slot 
					if(table[row][1] == '-')
					{
						table[row][1] = player;  //if slot =  '-' replace with player
						row=-1;
					}
				}
				slot_fill[0] +=1;
				break;
			}
			if(slot_check(slot_fill[0])==0) //if check full slot
			{
				cout << "This slot full ! Please select other slots."<<endl;
				player_in(table,player,slot_fill);
				break;
			}
			
		case 2 :
			if(slot_check(slot_fill[1])==1)
			{
				for(int row=6;row>=1;row--)
				{
					if(table[row][2] == '-')
					{
						table[row][2] = player;
						row=-1;
					}
				}
				slot_fill[1] +=1;
				break;
			}
			if(slot_check(slot_fill[1])==0)
			{
				cout << "This slot full ! Please select other slots."<<endl;
				player_in(table,player,slot_fill);
				break;
			}
		case 3 :
			if(slot_check(slot_fill[2])==1)
			{
				for(int row=6;row>=1;row--)
				{
					if(table[row][3] == '-')
					{
						table[row][3] = player;
						row=-1;
					}
				}
				slot_fill[2] +=1;
				break;
			}
			if(slot_check(slot_fill[2])==0)
			{
				cout << "This slot full ! Please select other slots."<<endl;
				player_in(table,player,slot_fill);
				break;
			}
		case 4 :
			if(slot_check(slot_fill[3])==1)
			{
				for(int row=6;row>=1;row--)
				{
					if(table[row][4] == '-')
					{
						table[row][4] = player;
						row=-1;
					}
				}
				slot_fill[3] +=1;
				break;			
			}
			if(slot_check(slot_fill[3])==0)
			{
				cout << "This slot full ! Please select other slots."<<endl;
				player_in(table,player,slot_fill);
				break;
			}
		case 5 :
			if(slot_check(slot_fill[4])==1)
			{
				for(int row=6;row>=1;row--)
				{
					if(table[row][5] == '-')
					{
						table[row][5] = player;
						row=-1;
					}
				}
				slot_fill[4] +=1;
				break;
			}
			if(slot_check(slot_fill[4])==0)
			{
				cout << "This slot full ! Please select other slots."<<endl;
				player_in(table,player,slot_fill);
				break;
			}
		case 6 :
			if(slot_check(slot_fill[5])==1)
			{
				for(int row=6;row>=1;row--)
				{
					if(table[row][6] == '-')
					{
						table[row][6] = player;
						row=-1;
					}
				}
				slot_fill[5] +=1;
				break;
			}
			if(slot_check(slot_fill[5])==0)
			{
				cout << "This slot full ! Please select other slots."<<endl;
				player_in(table,player,slot_fill);
				break;
			}
		case 7 :
			if(slot_check(slot_fill[6])==1)
			{
				for(int row=6;row>=1;row--)
				{
					if(table[row][7] == '-')
					{
						table[row][7] = player;
						row=-1;
					}
				}
				slot_fill[6] +=1;
				break;
			}
			if(slot_check(slot_fill[6])==0)
			{
				cout << "This slot full ! Please select other slots."<<endl;
				player_in(table,player,slot_fill);
				break;
			}
		default : //if not 1-7 let player re type
			cout<<"Please enter number 1-7"<<endl;
			player_in(table,player,slot_fill);
	
	}
}

char p_turn(int turn) //switch turns fuction
{
	char player; //change player by used int turn to controll
	
	if(turn == 0)
	{
		player='O';
	}
	if(turn == 1)
	{
		player='X';
	}
	
	return player; //return player
}

void s_turn(char table[][col]) //first turn function the main fuction that used switch player
{
	int turn=0,end_count=42,slot_fill[7]={0,0,0,0,0,0,0};
	char player = 'O';
	
	while(end_count > 0) //while turn > 0 start at 42 (6*7)
	{
		cout <<"Player "<<player<<" turn.Select column 1 to 7. : ";
		if(turn==0)
		{
			player_in(table,p_turn(turn),slot_fill);
			show_table(table);
			end_count -=1;
			turn=1;
			if(player == 'O')
			{
				end_count = win_check(table,end_count,player);
				player = 'X';
			}
			else if(player == 'X')
			{
				end_count = win_check(table,end_count,player);
				player = 'O';
			}
		}
		else
		{
			player_in(table,p_turn(turn),slot_fill);
			show_table(table);
			end_count -=1;
			turn=0;
			if(player == 'O')
			{
				end_count = win_check(table,end_count,player);
				player = 'X';
			}
			else if(player == 'X')
			{
				end_count = win_check(table,end_count,player);
				player = 'O';
			}
		}
	}
	
	if(end_count ==0) //if end count = 0 (on one win)
	{
		cout<<"Draw";
	}
	if(end_count ==-1) //if end count =-1 (someone win)
	{
		if(player == 'O')
		{
			player = 'X';
		}
		else if(player == 'X')
		{
			player = 'O';
		}
		cout<<"Player "<<player<<" Win!";
	}
	
}

char checK_h(char table[][col],char player) //horizontally win check function start from left bottum point and check all by horizontally
{
	char end_call;	
	for(int t_row=row-1; t_row>=0; t_row--)
	{
		for(int t_col=0; t_col<col; t_col++)
		{
			if(table[t_row][t_col] == table[t_row][t_col+1] && table[t_row][t_col] == table[t_row][t_col+2] && table[t_row][t_col] == table[t_row][t_col+3] && table[t_row][t_col] != '-')
			{
				end_call=player;
				return end_call;
			}
			else
			{
				end_call='U';
			}
		}
	}
	return end_call;
}

char checK_v(char table[][col],char player) //vertically win check function start from left bottum point and check all by vertically
{
	char end_call;
	for(int t_row=row-1; t_row>=0; t_row--)
	{
		for(int t_col=0; t_col<col; t_col++)
		{
			if(table[t_row][t_col] == table[t_row+1][t_col] && table[t_row][t_col] == table[t_row+2][t_col] && table[t_row][t_col] == table[t_row+3][t_col] && table[t_row][t_col] != '-')
			{
				end_call=player;
				return end_call;
			}
			else
			{
				end_call='U';
			}
		}
	}
	return end_call;
}

char checK_dup(char table[][col],char player) //up to down diagonally check function start from left top point and check all by diagonally
{
	char end_call;
	for(int t_row=0; t_row<row; t_row++)
	{
		for(int t_col=0; t_col<col; t_col++)
		{
			if(table[t_row][t_col] == table[t_row-1][t_col-1 ] && table[t_row][t_col] == table[t_row-2][t_col-2] && table[t_row][t_col] == table[t_row-3][t_col-3] && table[t_row][t_col] != '-')
			{
				end_call=player;
				return end_call;
			}
			else
			{
				end_call='U';
			}
		}
	}
	return end_call;
}

char checK_ddn(char table[][col],char player) //down to up diagonally check function start from left bottum point and check all by diagonally
{
	char end_call;
	for(int t_row=row-1; t_row>=0; t_row--)
	{
		for(int t_col=0; t_col<col; t_col++)
		{
			if(table[t_row][t_col] == table[t_row-1][t_col+1] && table[t_row][t_col] == table[t_row-2][t_col+2] && table[t_row][t_col] == table[t_row-3][t_col+3] && table[t_row][t_col] != '-')
			{
				end_call=player;
				return end_call;
			}
			else
			{
				end_call='U';
			}
		}
	}
	return end_call;
}

int win_check(char table[][col],int end_count,char player) //win check function the function that contain all check function
{
	char win='U';	
	if(win=='U')
	{
		win=checK_h(table,player);
		if(win=='U')
		{
			win=checK_v(table,player);
			if(win=='U')
			{
				win=checK_dup(table,player);
				if(win=='U')
				{
					win=checK_ddn(table,player);
					if(win=='U')
					{	
						return end_count;
					}
					else
						return -1;
				}
				else
					return -1;
			}
			else
				return -1;	
		}
		else
			return -1;
	}
	else		
		return -1;
}

int slot_check(int slot) //slot check fuction the function that check the slot that full or not
{
	if(slot<6)
	{
		return 1;
	}
	else
		return 0;
}